﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;

namespace Eliza.Web
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var root = Directory.GetCurrentDirectory();

            var config = new ConfigurationBuilder()
                .SetBasePath(root)
                .AddJsonFile("hosting.json", true)
                .Build();

            var host = new WebHostBuilder()
                .UseKestrel()
                .UseContentRoot(root)
                .UseStartup<Startup>()
                .UseIISIntegration()
                .UseConfiguration(config)
                .Build();

            host.Run();
            
        }
    }
}
